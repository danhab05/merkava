import os
import shutil
import subprocess


def pushToGit():
    os.chdir(currendDir)
    subprocess.run(["git", "add", "."])
    subprocess.run(["git", "commit", "-m", "auto commit"])
    subprocess.run(["git", "push", "origin", "main"])
def flutterDeploy():
    os.chdir("uiappimo")
    subprocess.call("flutter build web", shell=True)
    subprocess.call("firebase deploy", shell=True)
def buildMac():
    subprocess.call("python3 setup.py py2app -A", shell=True)
    # subprocess.call('pyinstaller --noconfirm  --onefile --icon /Users/snowy/Documents/Programmation/appimoauto/back/icon.icns   /Users/snowy/Documents/Programmation/appimoauto/back/facilitimo.py', shell=True)
    # subprocess.call('appify facilitimo.sh dist/facilitimo.app icon.icns', shell=True)
    subprocess.call("open dist", shell=True)
    # pyinstaller --noconfirm --icon /Users/snowy/Documents/Programmation/appimoauto/back/icon.ico  -F --add-data "/Users/snowy/Documents/Programmation/appimoauto/back/key.json:." /Users/snowy/Documents/Programmation/appimoauto/back/facilitimo.py
def buildWindows():
    subprocess.call('pyinstaller --noconsole  --onefile --noconfirm --windowed --icon  "icon.ico" "merkava.py"', shell=True)
    # subprocess.call('pyinstaller  --onefile --noconfirm   --add-binary "chromedriver.exe";"." --icon  "D:/Programmation/Python/appimoauto/back/icon.ico" --add-data "D:/Programmation/Python/appimoauto/back/key.json;." --add-data "D:/Programmation/Python/appimoauto/back/pictures;pictures/"  --add-data "D:/Programmation/Python/appimoauto/back/cookies;cookies/" "D:/Programmation/Python/appimoauto/back/facilitimo.py"', shell=True)
    subprocess.call("explorer.exe dist", shell=True)
    
currendDir = os.getcwd()
while True:
    q = input("\n1: Pull from git\n2: Add to git\n3: firebase deploy\n4: Firebase and git:\n5: Build windows:\n6: Build mac:\nYour choice: ")
    if q == "1":
        subprocess.run(["git", "pull", "origin", "main"])
    elif q == "2":
        pushToGit()
    elif q == "3":
        flutterDeploy()
    elif q == "4":
        flutterDeploy()
        pushToGit()
    elif q == "5":
        buildWindows()
    elif q == "6":
        buildMac()
    else:
        continue
    os.chdir(currendDir)
