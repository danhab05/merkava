import requests
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
import time
import os
import sys
from bs4 import BeautifulSoup
import PyPDF2
import wget
from pathlib import Path
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from time import sleep
merger = PyPDF2.PdfFileMerger()

def main():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    for i in range(58, 59):
        driver.get(f"https://hebrewbooks.org/shas.aspx?mesechta=19&daf={str(i)}&format=pdf")
        sleep(1)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        a = soup.find("iframe", {"id": "acroframe"}).find("a").get("href")
        a = a.split("#")[0]
        os.system(f"wsl wget {a} -O pdf/{str(i)}.pdf")

        driver.get(f"https://hebrewbooks.org/shas.aspx?mesechta=19&daf={str(i)}b&format=pdf")
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        a = soup.find("iframe", {"id": "acroframe"}).find("a").get("href")
        a = a.split("#")[0]
        os.system(f"wsl wget {a} -O pdf/{str(i)}b.pdf")

def merge():
    a = os.listdir("pdf")
    # sort files by name
    a.sort(key=lambda f: int(''.join(filter(str.isdigit, f))))
    for file in a:
        if file.endswith(".pdf"):
            merger.append("pdf/" + file)
    merger.write("combinedDocs.pdf")
# main()
merge()