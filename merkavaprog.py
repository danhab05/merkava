import platform
import urllib

from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from time import sleep
import os
from translate import Translator
from pprint import pprint
from bs4 import BeautifulSoup
from langdetect import detect
import urllib3
import shutil
VER_CHROME = "116.0.5845.96"

if platform.system() == "Windows":
    driver = webdriver.Chrome(ChromeDriverManager().install())
else:
    ver = "114.0.5735.90"
    if "arm" in platform.processor():

        driverFile = urllib.request.urlretrieve(
            f"https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/{VER_CHROME}/mac-arm64/chromedriver-mac-arm64.zip", os.getcwd() + "/chromedriver_mac_arm64.zip")
        shutil.unpack_archive(
            os.getcwd() + "/chromedriver_mac_arm64.zip", os.getcwd())
        
        os.chmod(os.getcwd() + "/chromedriver-mac-arm64/chromedriver", 754)
        os.rename(os.getcwd() + "/chromedriver-mac-arm64/chromedriver", "chromedriver")
    else:
        driverFile = urllib.request.urlretrieve(
            f"https://chromedriver.storage.googleapis.com/{ver}/chromedriver_mac64.zip",
            os.getcwd() + "/chromedriver_mac64.zip")
        shutil.unpack_archive(
            os.getcwd() + "/chromedriver_mac64.zip", os.getcwd())
    os.chmod(os.getcwd() + "/chromedriver", 754)
    driver = webdriver.Chrome(os.getcwd() + "/chromedriver")
    print("here")
driver.get("https://www.themercava.com/app/books/metanav/5883")
sleep(10)
while True:
    try:
        result = driver.execute_script('''
        txt = document.getElementsByClassName('subtitleTextWide')[0].textContent;
        return txt
        ''')
        if detect(result) != "fr":
            translation = Translator(to_lang="fr").translate(result)
            driver.execute_script(f'document.getElementsByClassName("subtitleTextWide")[0].replaceWith("{translation}")')
        sleep(0.1)
    except Exception as e:
        if "Cannot read properties of undefined" in str(e):
            pass
        else:
            print(e)
